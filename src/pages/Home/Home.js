import axios from "axios";
import { useEffect, useState } from "react";
import styles from "./Home.module.scss";
import CardVoyages from "Component/CardVoyages/CardVoyages";
import styled from "styled-components";

const CardVoyage = styled.div`
display: grid;
grid-template-columns: repeat(3, 1fr );
grid-template-rows: repeat(auto, 1fr );
margin: 5px 5px 20px 5px;
gap: 5%;
z-index: 100;
`;

export async function fetchAxios() {

  await axios

    .get("voyages.json")
    .then(function (response) {
   
      const { data } = response;
      console.log( data.voyages);
    })
    .catch(function (error) {
      
      console.log(error);
    })
    .finally(function () {
     
    });


}
function Voyages() {
      const [voyages, setVoyages] = useState([]);
    
      useEffect(() => {
        fetch("voyages.json")
          .then((response) => response.json())
          .then((data) => setVoyages(data.voyages))
          .catch((error) => console.error(error));
      }, []);
      
     
      return (
      <>
      <CardVoyage>
          {voyages.map((voyage) => (
           <CardVoyages pays={voyage.pays} prix={voyage.prix} date={voyage.date} description={voyage.description} image={voyage.image} key={voyage.id} className={styles.cardvoyage}/> 
            // <div key={voyage.pays} className={styles.cards}>
            //   <img src={voyage.image} alt=""className={styles.photos}></img>
            //   <h2>{voyage.pays}</h2>
            //   <p>{voyage.description}</p>
            //   <p>Prix : {voyage.prix} euros</p>
            // </div>
           
          ))}
      </CardVoyage>
          
        </>
      ); 
    }

export default Voyages;



