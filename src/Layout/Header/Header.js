import styles from './Header.module.scss';
import { Link } from 'react-router-dom';
function Header() {
    return (
        <>
            <header className={styles.main}>
                <div className={styles.container}>
                    <div className={styles.logo}>
                        <img src='logo.png' alt="logo" />
                    </div>
                    <nav className={styles.nav}>
                        <ul className={styles.list} >
                            <li><Link to="/" className={styles.link}>Home</Link></li>
                            <li><Link to="/services" className={styles.link}>Services</Link></li>
                            <li><Link to="/voyages" className={styles.link}>Voyages</Link></li>
                            <li><Link to="/contact" className={styles.link}>Contact</Link></li>
                        </ul>
                    </nav>
                </div>
            </header>

        </>
    );
}

export default Header;
