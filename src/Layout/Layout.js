import {Routes, Route} from 'react-router-dom';
import Home from 'pages/Home/Home';
import Services from 'pages/Services/Services';
import Voyages from 'pages/Voyages/Voyages';
import Contact from 'pages/Contact/Contact';
import Header from './Header/Header';
import Footer from './Footer/Footer';
//import Voyages from 'Api/VoyageAxios';  


function Layout() {
    return (
        <>
      <div className="Layout">
        <Header/>
        <Routes>
        <Route path="/" element={<Home />} />
        <Route path="Services" element={<Services/>} />
        <Route path="Voyages" element={<Voyages/>} />
        <Route path="Contact" element={<Contact/>} />
      </Routes>
        <br/>
      <Footer/>
      </div>
    </>
    );
  }
  
  export default Layout;
  