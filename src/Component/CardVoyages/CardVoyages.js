import styles from './CardVoyages.module.scss';
// import { Link } from 'react-router-dom';

function CardVoyages(props) {
    return (
        <>
            <div key={props.id} className={styles.cards}>
                <img src={props.image} alt="" className={styles.photos}/>
                <h2>{props.pays}</h2>
                <p><b>Déscription: </b><br />{props.description}</p>
                <p><b>Prix à partir de: {props.prix} euros</b> </p>
                <p><b>Nos dates: {props.date}</b></p>
                <div>
                    <button>Déscription</button>
                </div>
            </div>

        </>
    );
}

export default CardVoyages;