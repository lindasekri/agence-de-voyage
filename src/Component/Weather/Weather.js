import axios from "axios";
import React, {useState} from "react";


export default function Weather() {

    const [weather, setWeather] = useState([]);
    const [city, setCity] = useState('');
    const apiKey = process.env.REACT_APP_APIKEY;

    const apiCall = async (e) => {
        e.preventDefault()
        const apiKey = "9f836203ba6b163eede15b2382b542a4";
        const loc = e.target.elements.loc.value
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${loc}&appid=${apiKey}`;
        const req = axios.get(url);
        const res = await req;
        setWeather({
            
            temp: res.data.main.temp,
            city: res.data.name,
            
        })

        setCity(res.data.name)

    }

    //Converting K to C
    let k = weather.temp;
    let C = k - 273.15

    const Weath = () => {
        return <div>
            <div className="winfo">
                Weather information for {city}
                <hr></hr>
            </div>
            <div className="Weath">
                <div className="welement">
                    Temperature : {C.toFixed(2)} &#8451;
                </div>
            </div>
        </div>
    }
    return (<>
        <div className="weathhead">Weather Info</div>
        <div className="mainweather">
            <div className="weather">
                <form onSubmit={apiCall} className="form">
                    <input type="text" 
                     placeholder="city" 
                     name="loc" />
                    <button className="styles.btn">Search</button>
                </form>

                {weather && <Weath />}
            </div>
        </div>
    </>
    )
}
